
import { CustomersService } from './../customers.service';
import { Component,OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';


import { PredictService } from '../predict.service';
import { Post } from '../post';
import { Customer } from '../interfaces/customer';
import { Router } from '@angular/router';

@Component({
  selector: 'customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  userId;
  classify;
  text;
  prediction;
  posts$:Observable<Post>
  saved = [];
  dis= false;
  
  
  

  seeComments(id:string){
     this.predictService.getComment(id); 
    console.log(id);
    this.router.navigate(['/classify']);

    

  }
  add(title){
    this.customersService.addPost(this.userId, title);
    this.dis= true;
    console.log(this.dis);
    
  }
  // predict(years, income){
  //   console.log(years,income);
  //   this.predictService.classify(years,income).subscribe(
  //     res => {
  //       console.log(res);
     
  //     return this.prediction;
  //    })}
  showPrediction(){
    return this.prediction;
  }
  editstate = [];
  constructor(private customersService:CustomersService, public authService:AuthService,private predictService:PredictService, private router:Router ) { }

  deleteCustomer(id:string){
    this.customersService.deleteCustomer(this.userId, id);
    console.log(id);
  } 
  updateCustomer(customer:Customer){
    this.customersService.updateCustomer(this.userId, customer.id, customer.name, customer.years,customer.income)
  }
   
  ngOnInit(): void {
        this.authService.getUser().subscribe(
          user => {
            this.userId = user.uid;
        })
        this.posts$ = this.predictService.getPosts();
        
      }}
        









  

  
