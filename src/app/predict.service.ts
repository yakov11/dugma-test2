import { HttpClient } from '@angular/common/http';
import {  Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Post } from './post';




@Injectable({
  providedIn: 'root'
})
export class PredictService {
  private url =  "https://pm3syupnh3.execute-api.us-east-1.amazonaws.com/beta";
  private URL = "https://jsonplaceholder.typicode.com/posts/";
  
  text;
  comments$;
  classifyRes;
  uuu;

  getPosts():Observable<Post>{
    return this.http.get<Post>(this.URL); 
  }
  

  getComment(id):Observable<Post>{
    this.uuu = (`${this.URL}${id}/comments`);
    return this.http.get<Post>(this.uuu)
  }

  classify(text:string){
    let json = 
      {'text':text}
    
    let body = JSON.stringify(json);
    
    return this.http.post<any>(this.url, body).pipe(
      map(res => {
        let final:string = res;
        return res; 
      })
    )

  }

  sendComments(){
    console.log(this.uuu);
    return this.http.get<Post>(this.uuu)
    
  }

  constructor(private http:HttpClient, public router:Router) { }
  
}
