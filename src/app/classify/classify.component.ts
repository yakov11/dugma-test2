import { CustomersService } from './../customers.service';
import { PredictService } from './../predict.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Post } from '../post';

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {

  text;
  category;
  comments$:Observable<Post>
  comment;
  editstate = [];

 

    // classify(){
  //   this.predictService.classify(this.text, this.text).subscribe(
  //     res => {

  //     }
  //   )
  //   this.router.navigate(['/customers']);
  // }
  constructor(private predictService:PredictService, private router:Router) { }

  ngOnInit(): void {
    this.comments$ = this.predictService.sendComments();
    console.log(this.comments$);
    
    
  }

}
