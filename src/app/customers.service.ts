import { AngularFirestoreModule, AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  customersCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection=this.db.collection('users');

  getCustomers(userId):Observable<any[]>{
    this.customersCollection = this.db.collection(`users/${userId}/posts`);
    return this.customersCollection.snapshotChanges().pipe(
      map(
        collection =>collection.map(
          document=> {
            const data = document.payload.doc.data();
            data.id = document.payload.doc.id;
            return data; 
          }
        )  
     ))  
  }


  deleteCustomer(userId:string, id:string){
    this.db.doc(`users/${userId}/posts/${id}`).delete();
     
  }
  addPost(userId:string, title:string){
    const post = {title:title};
    this.userCollection.doc(userId).collection('posts').add(post);
  }

  public updateCustomer(userId:string, id:string, name:string, years:number, income:number){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        years:years,
        income:income,
      })}
      public updateLikes(userId:string, id:string, likes:number){
        this.db.doc(`users/${userId}/posts/${id}`).update(
          {
            likes:likes
          })}
      

  public addPredict(userId:string,id:string,prediction:string){
    this.db.doc(`users/${userId}/posts/${id}`).update(
      {
        prediction:prediction
      }
    )
    
  }

  addArticle(userId:string,text:string,classify:string){
    const article = {text:text, classify:classify}; 
    this.userCollection.doc(userId).collection('articles').add(article);
  }
  

  constructor(private db:AngularFirestore) { }
}







