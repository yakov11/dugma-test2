import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


import { Customer } from '../interfaces/customer';
import { Post } from '../post';

@Component({
  selector: 'customerform',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})

export class CustomerFormComponent implements OnInit {

@Input() postId;
@Input() body;
@Input() id;
@Output() update = new EventEmitter<any>()
@Output() closeEdit = new EventEmitter<null>()

hasNotError:boolean=true;

updateParent(){
  console.log(this.id);
  let post:Post = {id:this.id, postId:this.postId, body:this.body};
  this.update.emit(post); 
}

tellParentToClose(){
  this.closeEdit.emit(); 
}



validateAge(year:number){
  if(year>24 || year<0){
    this.hasNotError=false;
    console.log(this.hasNotError);
  };
}

onSubmit(){}


  constructor() { }

  ngOnInit(): void {
  }

}
