import { PredictService } from './../predict.service';
import { CustomersService } from './../customers.service';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  mail;
  userId;
  customers$
  predictAns:any;

  predictPost(title, id){
   
    this.predictService.classify(title).subscribe(
      res => {
        
        this.predictAns = res;
        console.log(this.predictAns);
        
}    )}
    

  addPrediction(title:string, id){
    this.customersService.addPredict(this.userId, id,this.predictAns);
  }
   
  deletePost(id){
    this.customersService.deleteCustomer(this.userId, id);
  }
  like(likes:number, id){
    if(likes == null){
      likes = 0;
    }
      this.customersService.updateLikes(this.userId, id, likes+1)
  }

  

  constructor(public authService:AuthService, public customersService:CustomersService, public predictService:PredictService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.mail = user.email;
        this.userId = user.uid;
        this.customers$ = this.customersService.getCustomers(this.userId);
            
        })}

  }

  
