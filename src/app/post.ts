export interface Post {
    postId?:number,
    userId?: number, 
    id: number,
    title?: string,
    body: string    
}
